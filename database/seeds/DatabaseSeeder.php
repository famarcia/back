<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'lucas',
            'email' => 'lucas@email.com',
            'auth_data' => '123',
        ]);
    }
}
