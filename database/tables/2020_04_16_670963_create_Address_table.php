<?php

use Aposoftworks\LOHM\Classes\Facades\LOHM;
use Aposoftworks\LOHM\Classes\Concrete\ConcreteTable as Table;

class create_Address_table {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {
        LOHM::table('address', function (Table $table) {
            $table->id();
            $table->morphs('link');
            $table->string('street');
            $table->string('neighborhood');
            $table->string('city');
            $table->string('number');
            $table->string('state');
            $table->string('complement')->nullable();
            $table->string('zipcode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        LOHM::dropTable('address');
    }
}
