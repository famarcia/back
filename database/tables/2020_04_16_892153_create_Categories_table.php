<?php

use Aposoftworks\LOHM\Classes\Facades\LOHM;
use Aposoftworks\LOHM\Classes\Concrete\ConcreteTable as Table;

class create_Categories_table {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {
        LOHM::table('categories', function (Table $table) {
           $table->id();
           $table->foreign("pharmacy_id", 'pharmacies')->onDelete('cascade');
           $table->string('code')->nullable();
           $table->string('label');
           $table->text('description')->nullable();
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        LOHM::dropTable('categories');
    }
}
