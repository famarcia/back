<?php

use Aposoftworks\LOHM\Classes\Facades\LOHM;
use Aposoftworks\LOHM\Classes\Concrete\ConcreteTable as Table;

class create_Brands_table {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {
        LOHM::table('brands', function (Table $table) {
            $table->id();
            $table->foreign("user_id", 'users')->onDelete('cascade');
            $table->string('label');
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        LOHM::dropTable('brands');
    }
}
