<?php

use Aposoftworks\LOHM\Classes\Facades\LOHM;
use Aposoftworks\LOHM\Classes\Concrete\ConcreteTable as Table;

class create_Documents_table {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {
        LOHM::table('documents', function (Table $table) {
            $table->id();
            $table->morphs('data');
            $table->text('url');
            $table->string('extension');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        LOHM::dropTable('documents');
    }
}
