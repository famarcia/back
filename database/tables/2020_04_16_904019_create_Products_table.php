<?php

use Aposoftworks\LOHM\Classes\Facades\LOHM;
use Aposoftworks\LOHM\Classes\Concrete\ConcreteTable as Table;

class create_Products_table {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {
        LOHM::table('products', function (Table $table) {
            $table->id();
            $table->foreign("pharmacy_id", 'pharmacies')->onDelete('cascade');
            $table->foreign("category_id", 'categories')->onDelete('cascade');
            $table->foreign("laboratory_id", 'laboratories')->onDelete('cascade');
            $table->string('label');
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        LOHM::dropTable('products');
    }
}
