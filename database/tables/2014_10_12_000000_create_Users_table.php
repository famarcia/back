<?php

use Aposoftworks\LOHM\Classes\Facades\LOHM as Schema;
use Aposoftworks\LOHM\Classes\Concrete\ConcreteTable as Blueprint;

class create_Users_table
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('auth_data')->nullable();
            $table->enum('auth_type', ['password', 'facebook','google'])->default('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
