<?php

use Aposoftworks\LOHM\Classes\Facades\LOHM;
use Aposoftworks\LOHM\Classes\Concrete\ConcreteTable as Table;

class create_Shopping_table {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {
        LOHM::table('purchases', function (Table $table) {
            $table->id();
            $table->foreign("motoboy_id", 'motoboys');
            $table->foreign("address_id", 'address');
            $table->foreign("client_id", 'clients');
            $table->morphs('delivery');
            $table->enum('status', ['paid', 'pending', 'delivering', 'delivered', 'canceled', 'denied'])->default('pending');
            $table->enum('payment',['onDelivery', 'creditCard', 'debitCard', 'billet'])->default('onDelivery');;
            $table->text('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        LOHM::dropTable('purchases');
    }
}
