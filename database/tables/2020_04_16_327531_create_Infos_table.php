<?php

use Aposoftworks\LOHM\Classes\Facades\LOHM;
use Aposoftworks\LOHM\Classes\Concrete\ConcreteTable as Table;

class create_Infos_table {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {
        LOHM::table('infos', function (Table $table) {
            $table->id();
            $table->morphs('data');
            $table->timestamp('birthday');
            $table->char('rg')->nullable();
            $table->char('cpf')->nullable();
            $table->char('cnpj')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        LOHM::dropTable('infos');
    }
}
