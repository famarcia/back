<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Auth
Route::post('login', 	'AuthController@login')->name('login');
Route::post('register', 'AuthController@register')->name('register');

Route::group(["middleware" => "auth:sanctum"], function () {

	//User
	Route::group(["prefix" => "/user"], function () {
		Route::get('/', 'User\Controller@show');

		//Add and control subuser types
		Route::dependencyResource("/motoboy", 	"Motoboy\Controller");
		Route::dependencyResource("/pharmacy", 	"Pharmacy\Controller");
	});

	//General resources
	Route::apiResource("/brands", "Brand\Controller");

	//Client resources
	Route::group(["prefix" => "/purchases"], function () {
		Route::get("/",				"Purchase\Controller@index");
		Route::post("/",			"Purchase\Controller@store");
		Route::get("/{purchase}",	"Purchase\Controller@show");
	});

	//Pharmacy resources
	Route::group(["prefix" => "/{pharmacy}"], function () {
		Route::apiResource("/products",		"Product\Controller");
		Route::apiResource("/categories", 	"Product\CategoryController");
		Route::apiResource("/laboratories",	"Laboratory\Controller");
	});
});
