<?php

namespace App\Exceptions;

use Exception;

class GeneralJsonException extends Exception {

    //-------------------------------------------------
    // Properties
    //-------------------------------------------------

    protected $message;
    protected $errorcode;
    protected $specificmessage;

    //-------------------------------------------------
    // Main functions
    //-------------------------------------------------
    public function __construct($specificmessage, $message = null, $errorcode = 500) {
        $this->message          = $message;
        $this->errorcode        = $errorcode;
        $this->specificmessage  = $specificmessage;
    }

    public function render () {
        return response()->json([
            "message" => $this->message? $this->message:"An error has occurred",
            "errors"=> [
                "name"=>[$this->specificmessage]
            ]
        ], $this->errorcode);
    }
}
