<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    protected $fillable = ['pharmacy_id', 'product_id', 'quantity'];

    public function pharmacy()
    {
        return $this->hasOne(Pharmacy::class, 'id','pharmacy_id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id','product_id');
    }
}
