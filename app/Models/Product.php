<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $fillable = ['pharmacy_id', 'category_id', 'laboratory_id', 'label', 'description'];

    public function pharmacy()
    {
        return $this->hasOne(Pharmacy::class,'id','pharmacy_id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id','category_id');
    }

    public function laboratory()
    {
        return $this->hasOne(Laboratory::class, 'id','laboratory_id');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'data');
    }

}
