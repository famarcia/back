<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Motoboy extends Model
{
    protected $fillable = ['user_id','label','description'];

    public function address()
    {
        return $this->morphOne(Address::class, 'link');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id','user_id');
    }

    public function info()
    {
        return $this->morphOne(Info::class, 'data');
    }

    public function documents()
    {
        return $this->morphMany(Document::class,'data');
    }
}
