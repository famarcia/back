<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    protected $fillable = ['url','extension'];

    public function motoboy()
    {
        return $this->morphTo('data');
    }
}
