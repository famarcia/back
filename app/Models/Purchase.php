<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    protected $fillable = ['motoboy_id','address_id', 'client_id','status','payment', 'data'];

    public function motoboy()
    {
        return $this->hasOne(Motoboy::class, 'id','motoboy_id');
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id');
    }

    public function pharmacy()
    {
        return $this->morphTo('delivery');
    }
}
