<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pharmacy extends Model
{
    protected $fillable = ['user_id', 'brand_id','label','description','crf'];

    public function address()
    {
        return $this->morphOne(Address::class, 'link');
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function brand()
    {
        return $this->hasOne(Brand::class,'id','brand_id');
    }

    public function members()
    {
        return $this->morphMany(Member::class, 'member');
    }

    public function info()
    {
        return $this->morphOne(Info::class, 'data');
    }

    public function purchases ()
    {
        return $this->morphMany(Purchase::class, 'delivery');
	}

	public function categories () {
		return $this->hasMany(Category::class);
	}

	public function products () {
		return $this->hasMany(Product::class);
	}

	public function laboratories () {
		return $this->hasMany(Laboratory::class);
	}
}
