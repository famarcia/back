<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    protected $fillable = ['street', 'neighborhood', 'city', 'number', 'state', 'complement', 'zipcode'];

    public function motoboy()
    {
        return $this->morphTo('link');
    }

    public function pharmacy()
    {
        return $this->morphTo( 'link');
    }

    public function client()
    {
        return $this->morphTo('link');
    }
}

