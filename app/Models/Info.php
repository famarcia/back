<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Info extends Model
{
    protected $fillable = ['rg','cpf','cnpj', "birthday"];

    public function pharmacy()
    {
        return $this->morphTo('data');
    }

    public function client()
    {
        return $this->morphTo('data');
    }

    public function motoboy()
    {
        return $this->morphTo('data');
    }
}
