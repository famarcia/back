<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    protected $fillable = ['pharmacy_id','link'];

    public function pharmacy()
    {
        return $this->belongsTo('pharmacy_id', 'pharmacy_id','id');
    }

    public function product()
    {
        return $this->morphTo('data');
    }
}
