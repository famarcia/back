<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
	protected $fillable = ['user_id', 'label','description'];

	public function purchases () {
		return $this->hasMany(Purchase::class);
	}

    public function user() {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function address() {
        return $this->morphOne(Address::class, 'link');
    }

    public function info() {
        return $this->morphOne(Info::class, 'data');
    }
}
