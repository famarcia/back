<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    protected $fillable = ['user_id', 'label','description'];

    public function user()
    {
        return $this->hasOne(User::class, 'id','user_id');
    }
}
