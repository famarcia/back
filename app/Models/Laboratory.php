<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Laboratory extends Model
{
    protected $fillable = ['pharmacy_id','label','description'];

    public function pharmacy()
    {
        return $this->hasOne(Pharmacy::class, 'id','pharmcy_id');
    }
}
