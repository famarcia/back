<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $fillable = ['pharmacy_id', 'code','label','description'];

    public function pharmacy()
    {
        return $this->hasOne(Pharmacy::class, 'id','pharmacy_id');
    }
}
