<?php


namespace App\Http\Repositories;


use Aposoftworks\LaravelUtilities\Abstractions\Repository;
use App\Models\Document;

class DocumentRepository extends Repository {
    public $model = Document::class;
}
