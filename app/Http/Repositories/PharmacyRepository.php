<?php


namespace App\Http\Repositories;


use Aposoftworks\LaravelUtilities\Abstractions\Relational\OneToOne;
use App\Exceptions\GeneralJsonException;
use App\Models\Brand;
use App\Models\Pharmacy;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PharmacyRepository extends OneToOne {
	public $parent 	= User::class;
	public $related = Pharmacy::class;

	public function index ($perPage = 25) {
		$data = Pharmacy::paginate($perPage);

		return $data;
	}

	public function store($user, $fields) {
		//User can only be in one pharmacy at the time
		if ($user->pharmacy) {
			throw new GeneralJsonException("Você já está cadastrado em uma farmacia", null, 405);
		}

		//Create brand if not present
		if (!Brand::find($fields["brand_id"])) {
			$brand 			= new Brand;
			$brand->label 	= $fields["brand_id"];
			$brand->user_id	= $user->id;
			$brand->save();

			//Update id
			$fields["brand_id"] = $brand->id;
		}

		//Create pharmacy
		$data = new Pharmacy;
		$data->fill($fields);
		$data->user_id 	= $user->id;

		//Save
		$data->save();

		//Response
		return $data;
	}
}
