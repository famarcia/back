<?php


namespace App\Http\Repositories;


use Aposoftworks\LaravelUtilities\Abstractions\Repository;
use App\Models\Stock;

class StockRepository extends Repository {
    public $model = Stock::class;
}
