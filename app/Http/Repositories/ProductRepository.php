<?php


namespace App\Http\Repositories;


use Aposoftworks\LaravelUtilities\Abstractions\Relational\OneToMany;
use App\Models\Pharmacy;
use App\Models\Product;

class ProductRepository extends OneToMany {
	public $parent 	= Pharmacy::class;
    public $related = Product::class;
}
