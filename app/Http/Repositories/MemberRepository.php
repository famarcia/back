<?php


namespace App\Http\Repositories;


use Aposoftworks\LaravelUtilities\Abstractions\Repository;
use App\Models\Member;

class MemberRepository extends Repository {
    public $model = Member::class;
}
