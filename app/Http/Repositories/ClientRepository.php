<?php


namespace App\Http\Repositories;


use Aposoftworks\LaravelUtilities\Abstractions\Repository;
use App\Models\Client;

class ClientRepository extends Repository {
    public $model = Client::class;
}
