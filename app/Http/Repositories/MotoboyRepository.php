<?php


namespace App\Http\Repositories;


use Aposoftworks\LaravelUtilities\Abstractions\Relational\OneToOne;
use App\Exceptions\GeneralJsonException;
use App\Models\Motoboy;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class MotoboyRepository extends OneToOne {
	public $parent 	= User::class;
	public $related = Motoboy::class;

	public function index ($perPage = 25) {
		$data = Motoboy::paginate($perPage);

		return $data;
	}

	public function store($user, $fields) {
		if ($user->motoboy) {
			throw new GeneralJsonException("Registro motoboy já presente", null, 405);
		}

		$data = new Motoboy;
		$data->fill($fields);
		$data->user_id = $user->id;

		//Default values if not given
		$data->label = isset($fields["label"]) ? $fields["label"]:$user->name;

		//Save
		$data->save();

		//Response
		return $data;
	}
}
