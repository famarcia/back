<?php


namespace App\Http\Repositories;


use Aposoftworks\LaravelUtilities\Abstractions\Repository;
use App\Models\Brand;

class BrandRepository extends Repository {
    public $model = Brand::class;
}
