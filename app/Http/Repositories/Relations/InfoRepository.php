<?php


namespace App\Http\Repositories\Relations;

use App\Models\Info;
use Aposoftworks\LaravelUtilities\Abstractions\Relational\OneToOne;
use App\Models\User;
use Carbon\Carbon;

class InfoRepository extends OneToOne {
	public $parent 	= User::class;
	public $related = Info::class;

	public function store($parent, $insert) {
		//Provide default
		if (!isset($insert["birthday"])) {
			$insert["birthday"] = Carbon::now();
		}

		//Already exists
		if ($parent->info()->exists()) {
			$info = $parent->info;
			$info->fill($insert);
			$info->save();
		}
		//Create new
		else {
			$info = $parent->info()->create($insert);
		}
	}
}
