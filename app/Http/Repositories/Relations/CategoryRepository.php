<?php


namespace App\Http\Repositories\Relations;

use Aposoftworks\LaravelUtilities\Abstractions\Relational\OneToMany;
use App\Models\Category;
use App\Models\Pharmacy;

class CategoryRepository extends OneToMany {
	public $parent 	= Pharmacy::class;
    public $related = Category::class;
}
