<?php


namespace App\Http\Repositories\Relations;

use App\Models\Address;
use Aposoftworks\LaravelUtilities\Abstractions\RepositoryBase;

class AddressRespository extends RepositoryBase {
	public $model = Address::class;
}
