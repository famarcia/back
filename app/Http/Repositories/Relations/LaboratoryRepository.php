<?php


namespace App\Http\Repositories\Relations;


use Aposoftworks\LaravelUtilities\Abstractions\Relational\OneToMany;
use App\Models\Laboratory;
use App\Models\Pharmacy;

class LaboratoryRepository extends OneToMany {
	public $parent 	= Pharmacy::class;
    public $related = Laboratory::class;
}
