<?php


namespace App\Http\Repositories;


use Aposoftworks\LaravelUtilities\Abstractions\Relational\OneToMany;
use App\Models\Client;
use App\Models\Purchase;

class PurchaseRepository extends OneToMany {
	public $related = Purchase::class;
	public $parent 	= Client::class;
}
