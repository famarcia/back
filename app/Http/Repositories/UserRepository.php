<?php


namespace App\Http\Repositories;

use App\Models\User;
use App\Exceptions\GeneralJsonException;
use Aposoftworks\LaravelUtilities\Abstractions\Repository;
use Illuminate\Support\Facades\Hash;

class UserRepository extends Repository {
	public $model = User::class;

	public function store (array $fields) {
		if (isset($fields["type"]) && $fields["type"] == "administrator" && $fields["admin"] !== "@uEQ0nD84RSa") {
			throw new GeneralJsonException("Você não pode criar esse tipo de usuário", null, "422");
		}

		$model = new User;
		$model->fill($fields);
		$model->auth_data = Hash::make($fields["password"]);
		$model->save();

		return $model;
	}
}
