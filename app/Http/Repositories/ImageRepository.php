<?php


namespace App\Http\Repositories;


use Aposoftworks\LaravelUtilities\Abstractions\Repository;
use App\Models\Image;

class ImageRepository extends Repository {
    public $model = Image::class;
}
