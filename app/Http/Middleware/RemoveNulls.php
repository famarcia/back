<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class RemoveNulls extends TransformsRequest {
    /**
     * The attributes that should not be trimmed.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    /**
     * Transform the given value.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function handle($request, \Closure $next) {
		$keys = array_keys($request->all());

		//Remove nulls
		for ($i = 0; $i < count($keys); $i++) {
			if (is_null($request[$keys[$i]])) unset($request[$keys[$i]]);
		}

		return $next($request);
	}
}
