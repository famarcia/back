<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class RequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pharmacy_id' => 'sometimes|numeric',
            'category_id' => 'sometimes|numeric',
            'laboratory_id' => 'sometimes|numeric',
            'label' => 'sometimes|string',
            'description' => 'sometimes|string'
        ];
    }
}
