<?php

namespace App\Http\Requests\Info;

use Illuminate\Foundation\Http\FormRequest;

class RequestCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rg' => 'required|string',
            'cpf' => 'required|string',
            'cnpj' => 'required|string'
        ];
    }
}
