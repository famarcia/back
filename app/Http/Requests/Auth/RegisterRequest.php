<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class RegisterRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request) {
		$rules = [];

		//User general fields
		$rules["name"] 		= 'required|string';
		$rules["email"] 	= 'required|email|unique:users';
		$rules["password"] 	= 'required|string|min:6|confirmed';
		$rules["type"] 		= 'sometimes|string|in:motoboy,client,pharmacy,administrator';

		//Specific fields
		if ($request->has("type")) {
			//Motoboy
			if ($request->type === "motoboy") {
				$rules["motoboy"] 				= 'required|array';
				$rules["motoboy.label"] 		= 'required|string|max:199';
				$rules["motoboy.description"] 	= 'required|string';
			}

			//Pharmacy
			if ($request->type === "pharmacy") {
				$rules["pharmacy"] 				= 'required|array';
				$rules["pharmacy.brand_id"] 	= 'required';
				$rules["pharmacy.label"] 		= 'required|string|max:199';
				$rules["pharmacy.description"] 	= 'required|string';
				$rules["pharmacy.crf"] 			= 'required';
			}

			//Admin
			if ($request->type === "admin") {
				$rules["admin"] 				= 'required|string|max:199';
			}
		}

		return $rules;
    }
}
