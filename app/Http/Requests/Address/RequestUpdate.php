<?php

namespace App\Http\Requests\Address;

use Illuminate\Foundation\Http\FormRequest;

class RequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'street' => 'sometimes|string',
            'neighborhood' => 'sometimes|string',
            'city' => 'sometimes|string',
            'number' => 'sometimes|string',
            'state' => 'sometimes|string',
            'complement' => 'sometimes|string',
            'zipcode' => 'sometimes|string'
        ];
    }
}
