<?php

namespace App\Http\Requests\Purchase;

use Illuminate\Foundation\Http\FormRequest;

class RequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'motoboy_id' => 'sometimes|numeric',
            'address_id' => 'sometimes|numeric',
            'client_id' => 'sometimes|numeric',
            'status' => 'sometimes|srting',
            'payment' => 'sometimes|string',
            'data' => 'sometimes|string'
        ];
    }
}
