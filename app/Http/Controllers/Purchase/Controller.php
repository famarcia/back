<?php

namespace App\Http\Controllers\Purchase;


use App\Http\Repositories\PurchaseRepository;
use App\Http\Requests\Purchase\RequestCreate;
use App\Http\Requests\Purchase\RequestUpdate;
use App\Http\Resources\Product\Collection;
use App\Http\Resources\Product\Resource;
use App\Models\Purchase;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController {

	public function index (Request $request, PurchaseRepository $repository) {
		$client = $request->user()->client;
		$data 	= $repository->index($client);

		return new Collection($data);
	}

	public function show (Purchase $purchase, Request $request, PurchaseRepository $repository) {
		$client = $request->user()->client;
		$data	= $repository->show($client, $purchase);

		return new Resource($data);
	}

	public function store (RequestCreate $request, PurchaseRepository $repository) {
		$client = $request->user()->client;
		$data 	= $repository->add($client, $request->validated());

		return new Resource($data);
	}
}
