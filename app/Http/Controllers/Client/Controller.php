<?php

namespace App\Http\Controllers\Client;



use Aposoftworks\LaravelUtilities\Abstractions\SmartControllerBase;
use App\Http\Repositories\ClientRepository;
use App\Http\Requests\Client\Request;
use App\Http\Resources\Client\Collection;
use App\Http\Resources\Client\Resource;
use App\Models\Client;

class Controller extends SmartControllerBase
{
    public $collection 		= Resource::class;
    public $resource 		= Collection::class;
    public $repository 		= ClientRepository::class;
    public $model 			= Client::class;
    //public $requestCreate 	= RequestCreate::class;
    //public $requestUpdate 	= RequestUpdate::class;
}
