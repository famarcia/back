<?php

namespace App\Http\Controllers\Motoboy;

use App\Http\Repositories\MotoboyRepository;
use App\Http\Requests\Motoboy\Request;
use App\Http\Resources\Motoboy\Collection;
use App\Http\Resources\Motoboy\Resource;
use App\Models\Motoboy;
use App\Http\Controllers\Controller as BaseController;
use App\Http\Repositories\Relations\InfoRepository;

class Controller extends BaseController {

	public function index (MotoboyRepository $repository) {
		$data = $repository->index();

		return new Collection($data);
	}

	public function show (MotoboyRepository $repository, Request $request, $motoboy) {
		$user 		= $request->user();
		$motoboy 	= $repository->show(is_null($motoboy) ? $user->motoboy:$motoboy);

		return new Resource($motoboy);
	}

	public function store (MotoboyRepository $repository, InfoRepository $info, Request $request) {
		$user = $request->user();
		$motoboy = $repository->store($user, []);

		//Data relation
		$info->store($motoboy, $request->validated());

		return new Resource($motoboy);
	}
}
