<?php

namespace App\Http\Controllers;

//General
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

//Model
use App\Models\User;

//Requests
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;

//Resources
use App\Http\Resources\User\Resource;

//Repositories
use App\Http\Repositories\UserRepository;

class AuthController extends Controller {

	//-------------------------------------------------
	// Main methods
    //-------------------------------------------------

    public function register(RegisterRequest $request, UserRepository $repository) {
        $type 	= $request->has("type") ? $request->type:'client';
        $fields	= $request->validated();

		//Create user
		$user 	= $repository->store($fields);

		//Save subclass
		if ($type !== "client")
			$user->{$type}()->create($fields[$type]);
		else
			$user->{$type}()->create([]);

		//Response
        return new Resource($user);
	}

    public function login(LoginRequest $request) {
        $fields = $request->validated();

        //Check if user exists
        $user = User::where("email", $fields["email"])->first();

        if(!$user) {
            return response()->json(["error"=>"Usuário não encontrado"], 404);
		}

        //Check if user uses password to login
        if ($user->auth_type != "password") {
            return response()->json(["error"=>"Você não pode fazer login utilizando senha"], 403);
		}

        //Check if password matches
        if (!Hash::check($fields["password"], $user->auth_data)) {
            return response()->json(["error"=>"Usuário não encontrado"], 404);
        }

        //Create tokens
        $token = $user->createToken('application');

        //Answer
        return response()->json([
            "token" => $token->plainTextToken,
            "data"  => new Resource($user)
        ]);
	}

    public function revoke (Request $request) {
        $user = $request->user();
        $user->tokens()->delete();

        //Return response
        return response()->json(["success"=>"Acesso apagado com sucesso"]);
    }
}
