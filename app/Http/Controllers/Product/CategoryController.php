<?php

namespace App\Http\Controllers\Product;

use App\Http\Repositories\Relations\CategoryRepository;
use App\Http\Requests\Category\RequestCreate;
use App\Http\Requests\Category\RequestUpdate;
use App\Http\Resources\Category\Collection;
use App\Http\Resources\Category\Resource;
use App\Models\Category;

use App\Http\Controllers\Controller;
use App\Models\Pharmacy;

class CategoryController extends Controller {

	public function index (CategoryRepository $repository, Pharmacy $pharmacy) {
		$data = $repository->index($pharmacy);

		return new Collection($data);
	}

	public function store (CategoryRepository $repository, Pharmacy $pharmacy, RequestCreate $request) {
		$data = $repository->add($pharmacy, $request->validated());

		return new Resource($data);
	}

	public function update (CategoryRepository $repository, Pharmacy $pharmacy, Category $category, RequestUpdate $request) {
		$data = $repository->update($pharmacy, $category, $request->validated());

		return new Resource($data);
	}

	public function destroy (CategoryRepository $repository, Pharmacy $pharmacy, Category $category) {
		$repository->destroy($pharmacy, $category);

		return response()->json(["success"=>""]);
	}
}
