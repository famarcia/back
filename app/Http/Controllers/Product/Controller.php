<?php

namespace App\Http\Controllers\Product;

use App\Http\Repositories\ProductRepository;
use App\Http\Requests\Product\RequestCreate;
use App\Http\Requests\Product\RequestUpdate;
use App\Http\Resources\Product\Collection;
use App\Http\Resources\Product\Resource;
use App\Models\Product;

use App\Http\Controllers\Controller as BaseController;
use App\Models\Pharmacy;

class Controller extends BaseController {

	public function index (ProductRepository $repository, Pharmacy $pharmacy) {
		$data = $repository->index($pharmacy);

		return new Collection($data);
	}

	public function store (ProductRepository $repository, Pharmacy $pharmacy, RequestCreate $request) {
		$data = $repository->add($pharmacy, $request->validated());

		return new Resource($data);
	}

	public function update (ProductRepository $repository, Pharmacy $pharmacy, Product $category, RequestUpdate $request) {
		$data = $repository->update($pharmacy, $category, $request->validated());

		return new Resource($data);
	}

	public function destroy (ProductRepository $repository, Pharmacy $pharmacy, Product $category) {
		$repository->destroy($pharmacy, $category);

		return response()->json(["success"=>""]);
	}
}
