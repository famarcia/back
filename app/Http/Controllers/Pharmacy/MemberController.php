<?php

namespace App\Http\Controllers\Member;


use Aposoftworks\LaravelUtilities\Abstractions\SmartControllerBase;
use App\Http\Repositories\MemberRepository;
use App\Http\Requests\Member\RequestCreate;
use App\Http\Requests\Member\RequestUpdate;
use App\Http\Resources\Member\Collection;
use App\Http\Resources\Member\Resource;
use App\Models\Member;

class MemberController extends SmartControllerBase {

    public $resource 		= Resource::class;
    public $collection 		= Collection::class;
    public $repository 		= MemberRepository::class;
    public $model 			= Member::class;
    public $requestCreate 	= RequestCreate::class;
    public $requestUpdate 	= RequestUpdate::class;
}
