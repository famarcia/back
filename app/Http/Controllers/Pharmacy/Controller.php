<?php

namespace App\Http\Controllers\Pharmacy;

use App\Http\Repositories\PharmacyRepository;
use App\Http\Requests\Pharmacy\Request;
use App\Http\Resources\Pharmacy\Collection;
use App\Http\Resources\Pharmacy\Resource;
use App\Models\Pharmacy;
use App\Http\Controllers\Controller as BaseController;
use App\Http\Repositories\Relations\InfoRepository;

class Controller extends BaseController {

	public function index (PharmacyRepository $repository) {
		$data = $repository->index();

		return new Collection($data);
	}

	public function show (PharmacyRepository $repository, Request $request, $motoboy) {
		$user 		= $request->user();
		$motoboy 	= $repository->show(is_null($motoboy) ? $user->motoboy:$motoboy);

		return new Resource($motoboy);
	}

	public function store (PharmacyRepository $repository, InfoRepository $info, Request $request) {
		$user 		= $request->user();
		$pharmacy 	= $repository->store($user, $request->validated());

		//Data relation
		$info->store($pharmacy, $request->validated());

		return new Resource($pharmacy);
	}
}
