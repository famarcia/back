<?php

namespace App\Http\Controllers\Laboratory;

use App\Http\Repositories\Relations\LaboratoryRepository;
use App\Http\Requests\Laboratory\RequestCreate;
use App\Http\Requests\Laboratory\RequestUpdate;
use App\Http\Resources\Laboratory\Collection;
use App\Http\Resources\Laboratory\Resource;
use App\Models\Laboratory;

use App\Http\Controllers\Controller as BaseController;
use App\Models\Pharmacy;

class Controller extends BaseController {

	public function index (LaboratoryRepository $repository, Pharmacy $pharmacy) {
		$data = $repository->index($pharmacy);

		return new Collection($data);
	}

	public function store (LaboratoryRepository $repository, Pharmacy $pharmacy, RequestCreate $request) {
		$data = $repository->add($pharmacy, $request->validated());

		return new Resource($data);
	}

	public function update (LaboratoryRepository $repository, Pharmacy $pharmacy, Laboratory $laboratory, RequestUpdate $request) {
		$data = $repository->update($pharmacy, $laboratory, $request->validated());

		return new Resource($data);
	}

	public function destroy (LaboratoryRepository $repository, Pharmacy $pharmacy, Laboratory $laboratory) {
		$repository->destroy($pharmacy, $laboratory);

		return response()->json(["success"=>""]);
	}
}
