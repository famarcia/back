<?php

namespace App\Http\Controllers\Address;

use Aposoftworks\LaravelUtilities\Abstractions\SmartControllerBase;
use App\Http\Repositories\Relations\AddressRespository;
use App\Http\Requests\Address\RequestCreate;
use App\Http\Requests\Address\RequestUpdate;
use App\Http\Resources\Address\Collection;
use App\Http\Resources\Address\Resource;
use App\Models\Address;

class Controller extends SmartControllerBase {

    public $resource 		= Resource::class;
    public $repository 		= AddressRespository::class;
    public $model 			= Address::class;
    public $collection 		= Collection::class;
    public $requestCreate 	= RequestCreate::class;
    public $requestUpdate 	= RequestUpdate::class;
}
