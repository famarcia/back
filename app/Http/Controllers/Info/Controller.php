<?php

namespace App\Http\Controllers\Info;

use Aposoftworks\LaravelUtilities\Abstractions\SmartControllerBase;
use App\Http\Repositories\Relations\InfoRepository;
use App\Http\Requests\Info\RequestCreate;
use App\Http\Requests\Info\RequestUpdate;
use App\Http\Resources\Info\Collection;
use App\Http\Resources\Info\Resource;
use App\Models\Info;

class Controller extends SmartControllerBase {

    public $resource 		= Resource::class;
    public $collection 		= Collection::class;
    public $repository 		= InfoRepository::class;
    public $model 			= Info::class;
    public $requestCreate 	= RequestCreate::class;
    public $requestUpdate 	= RequestUpdate::class;
}
