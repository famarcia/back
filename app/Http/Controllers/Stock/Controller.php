<?php

namespace App\Http\Controllers\Stock;


use Aposoftworks\LaravelUtilities\Abstractions\SmartControllerBase;
use App\Http\Repositories\StockRepository;
use App\Http\Requests\Stock\RequestCreate;
use App\Http\Requests\Stock\RequestUpdate;
use App\Http\Resources\Stock\Collection;
use App\Http\Resources\Stock\Resource;
use App\Models\Stock;

class Controller extends SmartControllerBase {

    public $resource 		= Resource::class;
    public $collection 		= Collection::class;
    public $repository 		= StockRepository::class;
    public $model 			= Stock::class;
    public $requestCreate 	= RequestCreate::class;
    public $requestUpdate 	= RequestUpdate::class;
}
