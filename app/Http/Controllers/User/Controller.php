<?php

namespace App\Http\Controllers\User;

//Model
use Model\User;

//General
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Support\Facades\Hash;

//Requests
use App\Http\Requests\User\UpdateRequest;
use App\Http\Requests\User\PasswordRequest;
use App\Http\Requests\User\PasswordUpdateRequest;

//Resources
use App\Http\Resources\User\Resource;

class Controller extends BaseController {

    //-------------------------------------------------
    // Main functions
    //-------------------------------------------------

    public function show (Request $request) {
        return new Resource($request->user());
    }

    public function update (UpdateRequest $request) {
        $user   = $request->user();
        $fields = $request->validated();

        //Save
        $user->fill($fields);
        $user->update();

        //Response
        return new Resource($user, ["success"=>"Usuário atualizado"]);
    }

    public function updatepassword (PasswordUpdateRequest $request) {
        $user   = $request->user();
        $fields = $request->validated();

        //Check if the user uses password
        if ($user->auth_type != "password") {
            return response()->json(["error"=>"Você não usa uma senha para conectar"], 403);
        }

        //Check if password matches
        if (!password_verify($fields["password_old"], $user->auth_data))
            return response()->json(["error"=>"Senha incorreta"], 403);

        //Save new password
        $user->auth_data = Hash::make($fields["password_new"]);

        $user->save();

        //Return response
        return response()->json(["success"=>"Senha atualizada"]);
    }

    public function delete (Request $request) {
        $user   = $request->user();
        $user->delete();

        //Return response
        return response()->json(["success"=>"Usuário apagado com sucesso"]);
    }
}
