<?php

namespace App\Http\Controllers\Brand;

use Aposoftworks\LaravelUtilities\Abstractions\SmartController\ApiController;
use App\Http\Repositories\BrandRepository;
use App\Http\Requests\Brand\RequestCreate;
use App\Http\Requests\Brand\RequestUpdate;
use App\Http\Resources\Brand\Collection;
use App\Http\Resources\Brand\Resource;
use App\Models\Brand;


class Controller extends ApiController {
    public $repository 		= BrandRepository::class;
    public $model 			= Brand::class;
	public $resource 		= Resource::class;
    public $collection 		= Collection::class;
    public $requestCreate 	= RequestCreate::class;
    public $requestUpdate 	= RequestUpdate::class;
}
