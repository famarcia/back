<?php

namespace App\Http\Resources\Motoboy;

use Illuminate\Http\Resources\Json\ResourceCollection;

class Collection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            'user' => $this->user,
            'created_at' => $this->date_created,
            'updated_at' => $this->date_updated,
            'deleted_at' => $this->date_deleted
        ];
    }
}
