<?php

namespace App\Http\Resources\Pharmacy;

use Illuminate\Http\Resources\Json\JsonResource;

class Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            'description' => $this->description,
            'client' => $this->client,
            'created_at' => $this->date_created,
            'updated_at' => $this->date_updated,
            'deleted_at' => $this->date_deleted
        ];
    }
}
