<?php

namespace App\Providers;

//General
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class MacroProvider extends ServiceProvider {

    public function register () {

		//Macros
		Route::macro("dependencyResource", function ($path, $controller) {
			Route::get($path,		$controller."@show");
			Route::post($path,		$controller."@store");
			Route::patch($path,		$controller."@update");
			Route::delete($path,	$controller."@destroy");
		});
    }
}
